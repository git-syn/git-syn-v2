/*
Copyright © 2024 Lucas Ramage <lucas.ramage@infinite-omicron.com>

*/
package main

import "gitlab.com/git-syn/git-syn/cmd"

func main() {
	cmd.Execute()
}
